torch>=2.0.1
matplotlib>=3.6.3,<4
pandas>=1.4.3,<2
scipy>=1.11.4
scikit-learn>=1.2.2,<2
numpy>=1.25.2,<2
joblib>=1.2.0
statsmodels>=0.13.5,<1
